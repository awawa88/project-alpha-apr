from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.form import ProjectForm

from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    projects_object = Project.objects.filter(owner=request.user)
    context = {"projects_object": projects_object}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project_object = get_object_or_404(Project, id=id)
    context = {
        "project_object": project_object,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            projects = form.save(False)
            projects.owner = request.user
            projects.save()

            return redirect("list_projects")

    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)
